module add cmake

module swap PrgEnv-cray PrgEnv-intel
module add cray-petsc/3.5.2.1 cray-tpsl
module add gcc/5.1.0

PREFIX=$PWD/local
export PKG_CONFIG_PATH=$PREFIX/lib/pkgconfig:$PKG_CONFIG_PATH
export PATH=$PREFIX/bin:$PATH
export PYTHONPATH=$PREFIX/lib64/python2.6/site-packages
export UNICORNLIBDIR=$PWD/unicorn

# UFC
cd ufc2-hpc
rm CMakeCache.txt 
cmake -DCMAKE_INSTALL_PREFIX:PATH=$PREFIX
make install
cd ..

# SymPy, Instant, FIAT, UFL, FFC, OrderedDict
for pkg in sympy-0.7.5 instant fiat ufl-1.0.0 ffc-1.0.0 ordereddict-1.1
do
  cd $pkg
  python setup.py install --prefix=$PREFIX
  cd ..
done

# DOLFIN-HPC
cd dolfin-hpc
sh regen.sh
CC=cc CXX=CC CFLAGS="-fast -no-ipo" CXXFLAGS=-"fast -no-ipo" ./configure --prefix=$PREFIX --with-pic --enable-function-cache --enable-optimize-p1 --disable-boost-tr1 --with-petsc --with-parmetis --enable-mpi --enable-mpi-io --disable-progress-bar --disable-xmltest --enable-ufl
make -j 8 install
cd ..
cp -av dolfin-hpc/site-packages/dolfin_utils $PREFIX/lib64/python2.6/site-packages
